.. :changelog:

Release History
---------------

dev
+++

**Improvements**

**Bugfixes**

0.2.5 (2017-11-14)
++++++++++++++++++

**Improvements**

- Moved from distutils to setuptools


0.2.0 (2017-11-14)
++++++++++++++++++

**Bugfixes**

- Fixed errors in setup.py


0.1.8 (2017-07-05)
++++++++++++++++++

**Improvements**

- Added methods getlightids and getgroupids to the Bridge class


0.1.7 (2017-07-03)
++++++++++++++++++

**Improvements**

- Added this changelog



0.1.6 (2017-07-03)
++++++++++++++++++

**Improvements**

- Split each class in seperate files.

**Bugfixes**

- Fixed the keywords section in setup.py



Pre 0.1.5
+++++++++

- Never thought of the idea to track changes... :)
